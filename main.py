

from mipsTranslate import MIPSrunner

if __name__ == '__main__':
    runner = MIPSrunner.MIPSrunner()
    with open('exampleMIPS.asm') as asm:
        asm_commands = asm.readlines()
    runner.LineCommand(asm_commands)

