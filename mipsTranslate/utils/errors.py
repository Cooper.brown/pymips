class MIPSMemoryError(Exception):
    def __init__(self, memory_val):
        super().__init__(f"The memory address {memory_val}  has not been defined")

    pass


class MIPSIncorrectTypeError(Exception):
    def __init__(self, memory_val, expected_type):
        super().__init__(f"The memory address {memory_val} is of incorrect type, should be of type {expected_type}")

    pass


class MIPSMissingCommands(Exception):
    def __init__(self, missing_val):
        super().__init__(f"there is no {missing_val} in file, cannot parce")

    pass


class MIPSInputBufferError(Exception):
    def __init__(self):
        super().__init__("The input buffer is missing or incorrect")

    pass
