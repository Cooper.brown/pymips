import mipsTranslate.registers as registers
from mipsTranslate.instruction_files import I_instuctions, R_instructions, memory_instructions, jump_branch
from mipsTranslate.utils.errors import MIPSMissingCommands, MIPSInputBufferError
from re import split


class MIPSrunner:
    def __init__(self):
        """
            Initialization
            memory: holds memory in type: Dictionary
            memoryStack: holds all commands in line order type: List
            textStack: holds all memory items type: List
            branchPoints: contains the names and line address of branch points type: List
        """
        self.register_class = registers.registers()
        self.memory = {'$sp':[0]}
        self.memoryStack = []
        self.textStack = []
        self.branchPoints = {}

    def LineCommand(self, asm_command_list: list):
        """
        :param asm_command_list: List of asm commands
        enumerates the list of commands separating the instructions from the memory definitions
        """
        # if asm_command_list.count(".text") != 1:
        #     raise MIPSMissingCommands(".text")
        for count, command in enumerate(asm_command_list):
            asm_command_list[count] = command.strip()
            v = command.strip()
            if v == '.text':
                self.textStack = asm_command_list[count + 1:]
                self.memoryStack = asm_command_list[:count]
            itm = split(" | ,", command.strip())
            if len(itm) == 1 and itm[0]:
                self.branchPoints[itm[0][:-1]] = count - (len(self.memoryStack))

        for command in self.memoryStack:
            self.dataCommands(command)

        self.text_commands(map(lambda s: s.strip(), self.textStack))

    def text_commands(self, command_list: list):
        """

        :param command_list: A list of commands, usually serialized by LineCommand
        Takes a list of commands, enumerates through them and passes the command to either
        self.branchPoints or self.typeCommands
        """
        for count, command in enumerate(command_list):
            broken_asm = split(" | ,", command)
            if len(broken_asm) == 1 and broken_asm[0]:
                self.branchPoints[broken_asm[0][:-1]] = count
            else:
                self.typeCommands(broken_asm, line=count)

    def typeCommands(self, broken_asm: list, line: int):
        """

        :param broken_asm: A list that contains each part of the mips command split
        :param line: the line number that is being processed, used in branching
        passes the command to its respective function
        """
        self.command = []
        for item in broken_asm:
            self.command.append(item.strip().replace(',', ''))
        if len(self.command) == 2:
            self.branch(self.command[0], self.command[1], line)
        elif len(self.command) != 1:
            if len(self.command) == 3:
                self.dataType(self.command[0], self.command[1], self.command[2])
            elif not self.command[3][0] == "$" and self.command[0][0] != "b":
                self.iType(self.command[0], self.command[1], self.command[2], self.command[3])
            elif self.command[0][0] == "b":
                self.branch(self.command[0], self.command[3], line, self.command[1], self.command[2])
            else:
                self.rType(self.command[0], self.command[2], self.command[3], self.command[1])
        else:
            self.syscall()
    def dataCommands(self, command: str):
        """
        :param command: a string containing a variable name and variable
        This function runs through the memory list and adds them to the self.memory variable
        """
        memory_list = command.split(":")
        if len(memory_list) <= 1:
            return
        elif len(memory_list[0]) <= 1:
            return
        self.memory[memory_list[0]] = list(filter(lambda item: item != '', split(" |, ", memory_list[1])))

    def rType(self, function: str, register1: str, register2: str, destination: str):
        """
        :param function: a string with the function given, exp: add
        :param register1: the first register in the command
        :param register2: the second register in the command
        :param destination: the register destination
        This function passes the command to the R_instruction function that
        corresponds with the function variable
        """
        if function == "and":
            function = "py_and"
        if function == "or":
            function = "py_or"
        function = getattr(R_instructions, function)
        function(register1, register2, destination, self.register_class)

    def iType(self, function: str, register1: str, register2: str, immediate: str):
        function = getattr(I_instuctions, function)
        function(register1, register2, immediate, self.register_class)

    def dataType(self, function, destination_register, memory_name):
        if "(" in memory_name:
            function = "list_" + function
            memory_name = memory_name.split("(")
            memory_name[1] = memory_name[1][:-1]
        function = getattr(memory_instructions, function)
        memory_return = function(destination_register, memory_name, self.register_class, self.memory)
        self.memory = memory_return

    def branch(self, function, jump_name, line, register1=None, register2=None):
        if function == "b" or "j":
            self.jump(jump_name, line)
        if function == "jal":
            self.jump(jump_name, line, link=True)
        if function == "jr":
            self.jump(jump_name, self.register_class.getRegister("$ra"))
        elif register2 is not None:
            function = getattr(jump_branch, function)
            function(jump_name, line, register1, register2, self.register_class, self)

    def jump(self, jump_name, line, link=False):
        try:
            jump_start = self.branchPoints[jump_name]
        except KeyError:
            jump_start = line
        if link:
            self.register_class.setRegister("$ra", line)
        self.text_commands(self.textStack[jump_start + 1:line])

    def syscall(self):
        # https://courses.missouristate.edu/kenvollmar/mars/help/syscallhelp.html
        instruction_int = self.register_class.getRegister("$v0")
        # Print syscall
        if (instruction_int <= 4) and (instruction_int != 0) or instruction_int == 11:
            item = self.register_class.getRegister("$a0")
            if type(item) == list:
                if item[0] == ".asciiz":
                    item.pop(0)
                item = ' '.join(item)
            print(item)
        # Int Input syscall
        elif (instruction_int > 4) and (instruction_int < 8) or instruction_int == 12:
            self.register_class.setRegister("$v0", input())
        # String Input syscall
        elif instruction_int == 4:
            usr_input = input()
            if len(usr_input) > self.register_class.getRegister("$a1"):
                raise MIPSInputBufferError
            self.register_class.setRegister("$a0", usr_input)
        # heap allocation
        elif instruction_int == 9:
            heap_size = self.register_class.getRegister("$a0")
            heap = [0] * heap_size
            self.register_class.setRegister("$v0", heap)
        # kill process
        elif instruction_int == 10 or instruction_int == 16:
            import sys
            sys.exit()
        # File input
        # open file
        elif instruction_int == 13:
            file_obj = self.register_class.getRegister("$a0")
            self.register_class.setRegister("$v0",open(file_obj))
        # read from file
        elif instruction_int == 14:
            file_obj = self.register_class.getRegister("$a0")
            self.register_class.setRegister("$v0", file_obj.read())
        # write to file
        elif instruction_int == 15:
            file_obj = self.register_class.getRegister("$a0")
            file_obj.write(self.register_class.getRegister("$a1"))
        # close file
        elif instruction_int == 16:
            file_obj = self.register_class.getRegister("$a0")
            file_obj.close()