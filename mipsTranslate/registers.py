

class registers:
    _registers = {
        # zero
        "$zero": 0,
        # pseudo instruction register
        "at": 0,
        # return registers
        "$v0": 0,
        "$v1": 0,
        # argument registers
        "$a0": 0,
        "$a1": 0,
        "$a2": 0,
        "$a3": 0,
        # temporary registers
        "$t0": 0,
        "$t1": 2,
        "$t2": 0,
        "$t3": 0,
        "$t4": 0,
        "$t5": 0,
        "$t6": 0,
        "$t7": 0,
        "$t8": 0,  # register number = 24
        "$t9": 0,  # register number = 25
        # saved registers
        "$s0": 0,
        "$s1": 0,
        "$s2": 0,
        "$s3": 0,
        "$s4": 0,
        "$s5": 0,
        "$s6": 0,
        "$s7": 0,
        # kernel registers
        "$k0": 0,
        "$k1": 0,
        # global area pointer
        "$gp": 0,
        # stack pointer
        "$sp": 0,
        # Frame pointer
        "$fp": 0,
        # return address
        "$ra": 0
    }

    def getRegister(self, register) -> int:
        return self._registers[str(register)]

    def setRegister(self, register, value):
        self._registers[str(register)] = value


