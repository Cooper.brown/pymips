import math


def addi(destination, register2, immediate, program_registers):
    register2 = program_registers.getRegister(register2)
    program_registers.setRegister(destination, register2 + int(immediate))


def addiu(destination, register2, immediate, program_registers):
    register2 = program_registers.getRegister(register2)
    calculated_num = register2 + int(immediate)
    if (calculated_num > 4_294_967_295) or (calculated_num < -4_294_967_295):
        raise OverflowError("Calculated num is over 32 bit limit")
    program_registers.setRegister(destination, register2 + int(immediate))


def andi(destination, register2, immediate, program_registers):
    register2 = program_registers.getRegister(register2)
    program_registers.setRegister(destination, register2 & int(immediate))


def ori(destination, register2, immediate, program_registers):
    register2 = program_registers.getRegister(register2)
    program_registers.setRegister(destination, register2 | int(immediate))


def sll(destination, register2, immediate, program_registers):
    register2 = program_registers.getRegister(register2)
    program_registers.setRegister(destination, register2 << int(immediate))


# I have chosen to avoid side effects with negative numbers with this function
def sra(destination, register2, immediate, program_registers):
    register2 = program_registers.getRegister(register2)
    program_registers.setRegister(destination, math.floor((abs(register2) / 2) * immediate))


def srl(destination, register2, immediate, program_registers):
    register2 = program_registers.getRegister(register2)
    if register2 >= 0:
        program_registers.setRegister(destination, register2 >> immediate)
    else:
        program_registers.setRegister(destination, (register2 + 0x100000000) >> immediate)


def xori(destination, register2, immediate, program_registers):
    register2 = program_registers.getRegister(register2)
    program_registers.setRegister(destination, int(int(register2) ^ int(immediate)))
