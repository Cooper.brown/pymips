from mipsTranslate.registers import registers


def beq(jump_name, line, register1, register2, register_class: registers, mips_class):
    if register_class.getRegister(register1) == register_class.getRegister(register2):
        mips_class.jump(jump_name, line)

def bge(jump_name, line, register1, register2, register_class: registers, mips_class):
    if register_class.getRegister(register1) >= register_class.getRegister(register2):
        mips_class.jump(jump_name, line)

def bgt(jump_name, line, register1, register2, register_class: registers, mips_class):
    if register_class.getRegister(register1) > register_class.getRegister(register2):
        mips_class.jump(jump_name, line)

def blt(jump_name, line, register1, register2, register_class: registers, mips_class):
    if int(register_class.getRegister(register1)) < int(register_class.getRegister(register2)):
        mips_class.jump(jump_name, line)

def ble(jump_name, line, register1, register2, register_class: registers, mips_class):
    if register_class.getRegister(register1) <= register_class.getRegister(register2):
        mips_class.jump(jump_name, line)

def bne(jump_name, line, register1, register2, register_class: registers, mips_class):
    if register_class.getRegister(register1) != register_class.getRegister(register2):
        mips_class.jump(jump_name, line)