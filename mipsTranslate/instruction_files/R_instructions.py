import math


def register_call_get(register1, register2, program_registers):
    register1 = program_registers.getRegister(register1)
    register2 = program_registers.getRegister(register2)
    return register1, register2


def add(register1, register2, destination, program_registers):
    register1, register2 = register_call_get(register1, register2, program_registers)
    if (type(register1) is list) or (type(register2) is list):
        try:
            register1.append(register2)
        except AttributeError:
            register2.append(register1)
        program_registers.setRegister(destination, register1 + register2)
        return
    calculated_num = int(register1) + int(register2)
    if not type(calculated_num) is list:
        if (calculated_num > 4_294_967_295) or (calculated_num < -4_294_967_295):
            raise OverflowError("Calculated num is over 32 bit limit")
    program_registers.setRegister(destination, calculated_num)


def addu(register1, register2, destination, program_registers):
    register1, register2 = register_call_get(register1, register2, program_registers)
    program_registers.setRegister(destination, register1 + register2)


def py_and(register1, register2, destination, program_registers):
    register1, register2 = register_call_get(register1, register2, program_registers)
    program_registers.setRegister(destination, int(register1) & int(register2))


def div(register1, register2, destination, program_registers):
    register1, register2 = register_call_get(register1, register2, program_registers)
    program_registers.setRegister(destination, int(register1) / int(register2))


def divu(register1, register2, destination, program_registers):
    register1, register2 = register_call_get(register1, register2, program_registers)
    if (register1 < 0) or (register2 < 0):
        raise OverflowError("divu can only be used for positive integers")
    program_registers.setRegister(destination, int(register1) / int(register2))


def mult(register1, register2, destination, program_registers):
    register1, register2 = register_call_get(register1, register2, program_registers)
    program_registers.setRegister(destination, int(register1) * int(register2))


def multu(register1, register2, destination, program_registers):
    register1, register2 = register_call_get(register1, register2, program_registers)
    if (register1 < 0) or (register2 < 0):
        raise OverflowError("divu can only be used for positive integers")
    program_registers.setRegister(destination, int(register1) * int(register2))


def nor(register1, register2, destination, program_registers):
    register1, register2 = register_call_get(register1, register2, program_registers)
    program_registers.setRegister(destination, ~(int(register1) | int(register2)))


def py_or(register1, register2, destination, program_registers):
    register1, register2 = register_call_get(register1, register2, program_registers)
    program_registers.setRegister(destination, int(register1) | int(register2))


def sllv(register1, register2, destination, program_registers):
    register1, register2 = register_call_get(register1, register2, program_registers)
    program_registers.setRegister(destination, int(register1) << int(register2))


# I have chosen to avoid side effects with negative numbers and this function
def srav(register1, register2, destination, program_registers):
    register1, register2 = register_call_get(register1, register2, program_registers)
    program_registers.setRegister(destination, math.floor((abs(register1) / 2) * register2))


def sub(register1, register2, destination, program_registers):
    register1, register2 = register_call_get(register1, register2, program_registers)
    program_registers.setRegister(destination, register1 - register2)


def subu(register1, register2, destination, program_registers):
    register1, register2 = register_call_get(register1, register2, program_registers)
    calculated_num = register1 - register2
    if (calculated_num > 4_294_967_295) or (calculated_num < -4_294_967_295):
        raise OverflowError("Calculated num is over 32 bit limit")
    program_registers.setRegister(destination, calculated_num)


def xor(register1, register2, destination, program_registers):
    register1, register2 = register_call_get(register1, register2, program_registers)
    program_registers.setRegister(destination, int(register1 ^ register2))
