from mipsTranslate.utils.errors import MIPSMemoryError, MIPSIncorrectTypeError


def lw(register, memory_var, program_registers, memory):
    if memory[str(memory_var)]:
        program_registers.setRegister(register, memory[str(memory_var)])
    else:
        raise MIPSMemoryError(str(memory_var))
    return memory


def list_lw(register, memory_var, program_registers, memory):
    a = memory_var
    memory_itm = memory[str(memory_var[1])]
    if type(memory_itm[0]) == list:
        memory_itm = memory_itm[0][0].split(",")
    a = len(memory_itm) >= int(memory_var[0])
    if memory_itm and len(memory_itm) > int(memory_var[0]):
        program_registers.setRegister(register, memory_itm[int(memory_var[0])])
    else:
        raise MIPSMemoryError(str(memory_var))
    return memory


def lb(register, memory_var, program_registers, memory):
    if memory[str(memory_var)]:
        program_registers.setRegister(register, memory[str(memory_var)])
    else:
        raise MIPSMemoryError(str(memory_var))
    return memory


def list_lb(register, memory_var, program_registers, memory):
    memory_itm = memory[str(memory_var[1])]
    if memory_itm and len(memory_itm) > int(memory_var[0]):
        program_registers.setRegister(register, memory_itm[int(memory_var[0])])
    else:
        raise MIPSMemoryError(str(memory_var))
    return memory


def lbu(register, memory_var, program_registers, memory):
    if memory[str(memory_var)] and type(memory[str(memory_var)]) is int:
        program_registers.setRegister(register, abs(memory[str(memory_var)]))
    elif not memory[str(memory_var)]:
        raise MIPSMemoryError(str(memory_var))
    else:
        raise MIPSIncorrectTypeError(str(memory_var), "int")
    return memory


def list_lbu(register, memory_var, program_registers, memory):
    memory_itm = memory[str(memory_var[1])]
    if memory_itm and (len(memory_itm) > int(memory_var[0])) and (memory_itm is int):
        program_registers.setRegister(register, abs(memory_itm))
    elif not memory_itm:
        raise MIPSMemoryError(str(memory_var))
    else:
        raise MIPSIncorrectTypeError(str(memory_var), "int")
    return memory


def sw(register, memory_var, program_registers, memory):
    memory[memory_var] = program_registers.getRegister(register)


def list_sw(register, memory_var, program_registers, memory):
    memory_list = memory[memory_var[1]]
    prog_reg = program_registers.getRegister(register)
    if len(memory_list) > int(memory_var[0]):
        memory_list[int(memory_var[0])] = prog_reg
    else:
        MIPSMemoryError(str(memory_var))
    memory[memory_var[1]] = memory_list
    return memory


