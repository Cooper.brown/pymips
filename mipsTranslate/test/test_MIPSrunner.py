from re import split
from unittest import mock
import pytest
from mipsTranslate.MIPSrunner import MIPSrunner
from mipsTranslate.utils.errors import MIPSMissingCommands



@mock.patch("mipsTranslate.MIPSrunner.MIPSrunner.text_commands")
def test_line_command_only_text(mocker, asm_commands_only_text):
    run = MIPSrunner()
    run.LineCommand(asm_commands_only_text)
    assert mocker.call_count == 1
    assert len(run.textStack) == 5


@mock.patch("mipsTranslate.MIPSrunner.MIPSrunner.dataCommands")
def test_line_command_only_data(mocker, asm_commands_only_data):
    run = MIPSrunner()
    run.LineCommand(asm_commands_only_data)
    assert mocker.call_count == len(run.memoryStack)


@mock.patch("mipsTranslate.MIPSrunner.MIPSrunner.dataCommands")
def test_line_command_corrupt_data(mocker, asm_commands_only_data):
    asm_commands_only_data.remove(".text")
    with pytest.raises(MIPSMissingCommands):
        run = MIPSrunner()
        run.LineCommand(asm_commands_only_data)


@mock.patch("mipsTranslate.MIPSrunner.MIPSrunner.typeCommands")
def test_text_commands(mocker):
    run = MIPSrunner()
    run.text_commands(["my_branch:"])
    assert run.branchPoints["my_branch"] == 0
    run.text_commands(["addi $s1, $s1, 100"])
    assert mocker.call_count == 1


def test_type_commands(asm_commands_only_text):
    run = MIPSrunner()
    expected = split(" | ,", asm_commands_only_text[-1].strip().replace(',', ''))
    run.typeCommands(split(" | ,", asm_commands_only_text[-1]), None)
    assert run.command == expected


@mock.patch("mipsTranslate.MIPSrunner.MIPSrunner.branch")
def test_type_commands_branch(mocker):
    run = MIPSrunner()
    run.typeCommands(["j", "my_branch"], 1)
    assert mocker.call_count == 1


@mock.patch("mipsTranslate.MIPSrunner.MIPSrunner.dataType")
def test_type_commands_data_type(mocker, asm_commands_data_type):
    run = MIPSrunner()
    run.typeCommands(split(" | ,", asm_commands_data_type[0].strip().replace(',', '')), 1)
    assert mocker.call_count == 1


def test_data_commands():
    run = MIPSrunner()
    run.dataCommands("total: 22")
    assert run.memory["total"] == ["22"]


def test_r_type():
    run = MIPSrunner()
    run.register_class.setRegister("$s3", 4)
    run.rType("add", "$s1", "$s3", "$s2")
    assert run.register_class.getRegister("$s2") == 4
    run.rType("and", "$s1", "$s3", "$s2")
    assert run.register_class.getRegister("$s2") == 0


def test_i_type():
    run = MIPSrunner()
    run.iType("addi", "$s2", "$s2", 2)
    assert run.register_class.getRegister("$s2") == 2


def test_data_type(asm_commands_only_data):
    run = MIPSrunner()
    run.LineCommand(asm_commands_only_data)
    run.dataType("lw", "$s2", "total")
    assert run.register_class.getRegister("$s2") == ['2']
    run.dataType("lw", "$s2", "3(my_array)")
    assert run.register_class.getRegister("$s2") == "27"



