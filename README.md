# MIPY



## About
This project aims to make a 100% python MIPS emulator, just for fun
I would not recommend using this for actual MIPS programing, for that use [mars](https://courses.missouristate.edu/KenVollmar/MARS/) from missori state 
This project is extremely loosely based on [this](https://github.com/CQU-AI/pymips) project by CQU-AI

## How to use

Note: The main.py in combination with file1.asm file is an example of how to utilize this program
- Create a MIPSrunner class instance
- pass a list of MIPS commands to the function LineCommand() in MIPSrunner
- watch it go

## Credits
- Made by [Cooper Brown](https://www.cooperbrowns.com/)
    - Drake University
    - You can email me through this alias email: berechnet_bomber@simplelogin.com
