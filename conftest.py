from pytest import fixture


@fixture
def asm_commands_only_text():
    return [
        ".text",
        "li $s0, 1",
        "li $t0, 10",
        "li $t1, 1",
        "li $t2, 0",
        "and $t3, $s2, $s1"
    ]


@fixture
def asm_commands_only_data():
    return [
        ".data",
        "total: 2",
        "my_array: 10, 6, 0, 27, 92, 18, 42",
        'newline:.asciiz "\n"',
        'return:.asciiz "the total is"',
        ".text"
    ]


@fixture
def asm_commands_data_type():
    return ["sw $s1 total"]
